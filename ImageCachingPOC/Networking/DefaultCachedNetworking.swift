import Foundation

class DefaultCachedNetworking: Networking {

    private let cacheBaseURL: URL
    private let memomryCacheSize: Int
    private let diskCacheSize: Int
    private let connectionPoolSize: Int

    lazy var cache: URLCache = {
        let cachesURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let diskCacheURL = cachesURL.appendingPathComponent("DownloadCache")
        let cache = URLCache(memoryCapacity: self.memomryCacheSize, diskCapacity: self.diskCacheSize, directory: diskCacheURL)
        return cache
    }()

    lazy var session: URLSession = {
        let config = URLSessionConfiguration.default
        config.urlCache = cache
        config.httpMaximumConnectionsPerHost = self.connectionPoolSize
        return URLSession(configuration: config)
    }()

    init(memorySize: Int = 100_000_000, diskSize: Int = 1_000_000_000, connectionPoolSize: Int = 10) {
        self.cacheBaseURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        self.memomryCacheSize = memorySize
        self.diskCacheSize = diskSize
        self.connectionPoolSize = connectionPoolSize
    }

    func get(url: URL, completion: @escaping NetworkingCompletion) {
        let request = URLRequest(url: url)
        let downloadTask = session.downloadTask(with: request) { [weak self] url, response, error in
            guard let self = self else { return }

            guard let response = response,
                  let url = url,
                  self.cache.cachedResponse(for: request) == nil,
                  let data = try? Data(contentsOf: url, options: [.mappedIfSafe]) else {
                      completion(self.cache.cachedResponse(for: request)!.data, response, error)
                      return
            }

            let cacheUrl = self.cacheBaseURL.appendingPathComponent(url.lastPathComponent)
            self.cache.storeCachedResponse(CachedURLResponse(response: response, data: data), for: request)

            _ = try? FileManager.default.replaceItemAt(cacheUrl, withItemAt: url)

            completion(data, response, error)
        }
        downloadTask.resume()
    }
}

