import Foundation

class DefaultNetworking: Networking {

    private let connectionPoolSize: Int

    lazy private var session: URLSession = {
        let config = URLSessionConfiguration.default
        config.httpMaximumConnectionsPerHost = self.connectionPoolSize
        return URLSession(configuration: config)
    }()

    init(connectionPoolSize: Int = 10) {
        self.connectionPoolSize = connectionPoolSize
    }

    private func performRequest(_ request: URLRequest, completion: @escaping NetworkingCompletion) {
        let dataTask = session.dataTask(with: request, completionHandler: completion)
        dataTask.resume()
    }

    func get(url: URL, completion: @escaping NetworkingCompletion) {
        var request = URLRequest(url: url)
        request.httpMethod = HTTPRequestMethod.get
        performRequest(request, completion: completion)
    }
}
