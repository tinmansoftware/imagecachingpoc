import Foundation

typealias NetworkingCompletion = ((_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void)

enum HTTPRequestMethod {
    static let get: String = "GET"
    static let post: String = "POST"
}

protocol Networking {
    func get(url: URL, completion: @escaping NetworkingCompletion)
}
