import Foundation

class Timer {

    private var then: Date!

    func start() {
        then = Date()
    }

    func current() -> TimeInterval {
        return Date().timeIntervalSince(then)
    }

    func stop() -> TimeInterval {
        return Date().timeIntervalSince(then)
    }
}
