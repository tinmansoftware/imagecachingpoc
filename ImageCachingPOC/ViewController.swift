import UIKit

class ViewController: UIViewController {

    enum ImageSize: String {
        case small = "iOSSmall"
        case medium = "iOSMed"
        case large = "iOSLarge"
    }

    private let networking: Networking = DefaultNetworking(connectionPoolSize: 10)
    private let uncachedNetworking: Networking = DefaultNetworking(connectionPoolSize: 10)
    private let cachedNetworking: Networking = DefaultCachedNetworking(connectionPoolSize: 10)

    private var images: [URL] = []
    private var uncachedImages: [URL] = []

    private lazy var allEditionImageUrls: [URL] = {
        guard let imageUrlsFilePath = Bundle.main.url(forResource: "edition_image_urls", withExtension: "json") else {
            fatalError("Failed to get a filepath for the image url data")
        }
        guard let imageUrlsData = try? Data(contentsOf: imageUrlsFilePath, options: Data.ReadingOptions.alwaysMapped) else {
            fatalError("Failed to parse image urls into data")
        }

        guard
            let imageUrlsJson = try? JSONSerialization.jsonObject(with: imageUrlsData, options: .fragmentsAllowed) as? [String: Any],
            let imageUrls = imageUrlsJson["images"] as? [String] else {
                fatalError("Failed to parse image url data into an array")
            }

        return imageUrls.compactMap { urlString in
            return URL(string: urlString)
        }
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchImageTar(size: .small, cached: false)
        fetchImageTar(size: .medium, cached: false)
        fetchImageTar(size: .large, cached: false)

        fetchImageTar(size: .small, cached: true)
        fetchImageTar(size: .medium, cached: true)
        fetchImageTar(size: .large, cached: true)

        fetchAllImages(from: allEditionImageUrls)
        fetchAllImagesUncached(from: allEditionImageUrls)
    }

    private func fetchImageTar(size: ImageSize, cached: Bool) {
        let imageBundleBase: String = "https://feeds.thetimes.co.uk/smapi-images/prod/edition_2022-03-04"
        let imageBundleUrl: URL = URL(string: "\(imageBundleBase)_\(size.rawValue).tar")!

        print("Starting fetch from \(imageBundleUrl.absoluteString) \(cached ? "(cached)" : "(uncached)")")

        let timer = Timer()
        timer.start()
        fetchImage(from: imageBundleUrl, cached: cached) { data, response, error in
            if error != nil {
                print("Error fetching from \(imageBundleUrl): \(error!.localizedDescription)")
                return
            }

            if let data = data {
                print("Fetched \(data.count) bytes from \(imageBundleUrl) in \( String(format: "%2.2f", timer.stop()) )s \(cached ? "(cached)" : "(uncached)")")
            }
        }
    }

    private func fetchImage(from url: URL, cached: Bool, completion: @escaping NetworkingCompletion) {
        let service: Networking = cached ? cachedNetworking : networking
        service.get(url: url, completion: completion)
    }

    private func fetchAllImagesUncached(from urlList: [URL]) {
        func output() {
            if uncachedImages.count == urlList.count {
                print("All images (\(uncachedImages.count)) fetched in \( String(format: "%2.2f", timer.stop()) )s (uncached)")
            } else if self.uncachedImages.count % 100 == 0 {
                //                print("Fetched \(self.uncachedImages.count) images (uncached)")
            }
        }

        print("Starting uncached fetch for \(urlList.count) images")

        let timer = Timer()
        timer.start()

        for url in urlList {
            uncachedNetworking.get(url: url) { [weak self] data, response, error in
                guard let self = self else { return }

                if error != nil {
                    print("Error fetching from \(url): \(error!.localizedDescription)")
                    return
                }

                guard let imageData = data,
                      let _ = UIImage(data: imageData) else {
                          return
                      }

                self.uncachedImages.append(url)
                output()
            }
        }
    }

    private func fetchAllImages(from urlList: [URL]) {
        func output() {
            if images.count == urlList.count {
                print("All images (\(images.count)) fetched in \( String(format: "%2.2f", timer.stop()) )s (cached)")
            } else if self.images.count % 100 == 0 {
                //                print("Fetched \(self.images.count) images (cached)")
            }
        }

        print("Starting cached fetch for \(urlList.count) images")

        let timer = Timer()
        timer.start()

        for url in urlList {
            cachedNetworking.get(url: url) { [weak self] data, response, error in
                guard let self = self else { return }

                if error != nil {
                    print("Error fetching from \(url): \(error!.localizedDescription)")
                    return
                }

                guard let imageData = data,
                      let _ = UIImage(data: imageData) else {
                          return
                      }

                self.images.append(url)
                output()
            }
        }
    }
}

